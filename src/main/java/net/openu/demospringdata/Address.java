package net.openu.demospringdata;

import javax.persistence.Embeddable;

@Embeddable
public class Address {
    private String state;
    private String city;
    private String street;
    private String zipcode;
}
