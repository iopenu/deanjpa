package net.openu.demospringdata;

import net.openu.demospringdata.domain.Comment;
import net.openu.demospringdata.domain.Post;
import org.hibernate.Session;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Component
@Transactional
public class JpaRunner implements ApplicationRunner {

    @PersistenceContext
    EntityManager entityManager;

    @Override
    public void run(ApplicationArguments args) throws Exception {




        Session session = entityManager.unwrap(Session.class);
        Post post = session.get(Post.class,1l);

        session.delete(post);


    }
}
