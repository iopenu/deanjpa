package net.openu.demospringdata;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
public class Account {
    @Id
    @GeneratedValue
//    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false,unique = true)
    private String username;

    private String password;

    @OneToMany(mappedBy = "owner")
    Set<Study> studies = new HashSet<>();

    public void addStudy(Study study) {
        this.getStudies().add(study);  //optional
        study.setOwner(this);
    }
    public void removeStudy(Study study) {
        this.getStudies().remove(study);  //optional
        study.setOwner(null);
    }


//    @Embedded
//    private Address homeAddress;


}
